#include <iostream>
#include <list>

using namespace std;

class Stack {
	list<int> store;
public:
	Stack() {
	}

	void push(int number) {
		store.push_back(number);
	}

	int pop() {
		int returnedNum = store.back();
		
		store.pop_back();

		return returnedNum;
	}

	void printToConsole() {
		for(list<int>::iterator iterator = store.begin(); iterator != store.end(); iterator++) {
			cout << (*iterator) << " ";
		}

		cout << endl;
	}
};

class Queue {
	list<int> store;
public: 
	Queue() {}

	void push(int number) {
		store.push_back(number);
	}

	int pop(){
		int returnedNum = store.front();
		
		store.pop_front();

		return returnedNum;
	}

	void printToConsole() {
		for(list<int>::iterator iterator = store.begin(); iterator != store.end(); iterator++) {
			cout << (*iterator) << " ";
		}

		cout << endl;
	}
};


int main()
{
	Stack stack;

	stack.push(1);
	stack.push(3);
	stack.push(5);

	cout << "stack" << endl << "before" << endl;
	stack.printToConsole();
	
	stack.pop();

	cout << "after" << endl;
	stack.printToConsole();

	system("pause");

	Queue queue;

	queue.push(1);
	queue.push(3);
	queue.push(5);

	cout << "queue" << endl << "before" << endl;
	queue.printToConsole();
	
	queue.pop();

	cout << "after" << endl;
	queue.printToConsole();

	system("pause");

	return 0;
}

